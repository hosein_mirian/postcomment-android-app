package tellh.com.recyclertreeview.bean;

import tellh.com.recyclertreeview.R;
import tellh.com.recyclertreeview_lib.LayoutItemType;

/**
 * Created by tlh on 2016/10/1 :)
 */
public class QA implements LayoutItemType {
    public String title;
    public String description;

    public QA(String title, String description) {
        this.title = title;
        this.description = description;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_dir;
    }
}