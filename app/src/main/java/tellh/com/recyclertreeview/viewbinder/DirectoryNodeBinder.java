package tellh.com.recyclertreeview.viewbinder;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tellh.com.recyclertreeview.AppController;
import tellh.com.recyclertreeview.R;
import tellh.com.recyclertreeview.bean.QA;
import tellh.com.recyclertreeview_lib.TreeNode;
import tellh.com.recyclertreeview_lib.TreeViewBinder;

/**
 * Created by tlh on 2016/10/1 :)
 */

public class DirectoryNodeBinder extends TreeViewBinder<DirectoryNodeBinder.ViewHolder> {
    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    public int total;

    @Override
    public void bindView(ViewHolder holder, int position, TreeNode node) {
        holder.ivArrow.setRotation(0);
        holder.ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_right_black_18dp);
        int rotateDegree = node.isExpand() ? 90 : 0;
        holder.ivArrow.setRotation(rotateDegree);
        QA dirNode = (QA) node.getContent();
        if (dirNode.title == null) {


            holder.title.setText(" " + "               " + recursiveCounter(node));
        } else {
        /*        total = node.getChildList().size();
                for (int i = 0; i < node.getChildList().size(); i++) {
                    TreeNode subNode = (TreeNode) node.getChildList().get(i);
                    total += subNode.getChildList().size();
                    for (int j = 0; j < subNode.getChildList().size(); j++) {
                        TreeNode subNode2 = (TreeNode) subNode.getChildList().get(j);
                        total += subNode2.getChildList().size();
                    }
                }*/
            holder.title.setText(dirNode.title + "               " + recursiveCounter(node));
        }
        holder.description.setText(dirNode.description);
        if (dirNode.title == null) {
            Drawable AnswerImage = AppController.context.getResources().getDrawable(R.drawable.a);
            holder.title.setCompoundDrawablesWithIntrinsicBounds(AnswerImage, null, null, null);
            holder.description.setTextColor(Color.rgb(100, 100, 100));

        } else {
            Drawable QuestionImage = AppController.context.getResources().getDrawable(R.drawable.q);
            holder.title.setCompoundDrawablesWithIntrinsicBounds(QuestionImage, null, null, null);
            holder.description.setTextColor(Color.rgb(255, 0, 0));
        }
        if (node.isLeaf())
            holder.ivArrow.setVisibility(View.INVISIBLE);
        else holder.ivArrow.setVisibility(View.VISIBLE);


    }

    private int recursiveCounter(TreeNode node) {
        total = node.getChildList().size();
        for (int i = 0; i < node.getChildList().size(); i++) {
            TreeNode subNode = (TreeNode) node.getChildList().get(i);
            total += subNode.getChildList().size();
            for (int j = 0; j < subNode.getChildList().size(); j++) {
                TreeNode subNode2 = (TreeNode) subNode.getChildList().get(j);
                total += subNode2.getChildList().size();
            }
        }
        return total;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_dir;
    }

    public static class ViewHolder extends TreeViewBinder.ViewHolder {
        private ImageView ivArrow;
        private TextView title;
        private TextView description;

        public ViewHolder(View rootView) {
            super(rootView);
            this.ivArrow = (ImageView) rootView.findViewById(R.id.iv_arrow);
            this.title = (TextView) rootView.findViewById(R.id.title);
            this.description = (TextView) rootView.findViewById(R.id.description);
        }

        public ImageView getIvArrow() {
            return ivArrow;
        }

        public TextView getTitle() {
            return title;
        }

        public TextView getDescription() {
            return description;
        }
    }
}
