package tellh.com.recyclertreeview;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tellh.com.recyclertreeview.bean.QA;
import tellh.com.recyclertreeview.viewbinder.DirectoryNodeBinder;
import tellh.com.recyclertreeview.viewbinder.FileNodeBinder;
import tellh.com.recyclertreeview_lib.TreeNode;
import tellh.com.recyclertreeview_lib.TreeViewAdapter;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv;
    private TreeViewAdapter adapter;
    Dialog dialogAddPost;
    Dialog dialogAddComment;
    private List<TreeNode> nodes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogAddPost = new Dialog(MainActivity.this);
                dialogAddPost.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogAddPost.setContentView(R.layout.dialogpost);
                dialogAddPost.show();
                final TextView title = (TextView) dialogAddPost.findViewById(R.id.title_input);
                final TextView description_input = (TextView) dialogAddPost.findViewById(R.id.description_input);
                Button postBt = (Button) dialogAddPost.findViewById(R.id.post_bt);
                postBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TreeNode<QA> q = new TreeNode<>(new QA(title.getText().toString(), description_input.getText().toString()));
                        nodes.add(q);

                        rv.setLayoutManager(new LinearLayoutManager(AppController.context));

                        adapter = new TreeViewAdapter(nodes, Arrays.asList(new FileNodeBinder(), new DirectoryNodeBinder()));

                        // whether collapse child nodes when their parent node was close.
//                      adapter.ifCollapseChildWhileCollapseParent(true);
                        adapter.setOnTreeNodeListener(new TreeViewAdapter.OnTreeNodeListener() {
                            @Override
                            public boolean onClick(final TreeNode node, final RecyclerView.ViewHolder holder) {
//                        Toast toast = Toast.makeText(getApplicationContext(),
//                        node.getChildList()+"",
//                        Toast.LENGTH_SHORT);
//                        toast.show();
                                Log.i("hos",""+node.getChildList().size());
                                Log.i("hos",""+node.getChildList());

                                dialogAddComment = new Dialog(MainActivity.this);
                                dialogAddComment.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialogAddComment.setContentView(R.layout.dialogcomment);
                                dialogAddComment.show();
                                final TextView description_input = (TextView) dialogAddComment.findViewById(R.id.description_input);
                                Button postBt = (Button) dialogAddComment.findViewById(R.id.post_bt);
                                postBt.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        TreeNode<QA> a = new TreeNode<>(new QA(null, description_input.getText().toString()));
                                        // passing null as title means its an answer nod.
                                        node.addChild(a);
                                        adapter.notifyDataSetChanged();
                                        dialogAddComment.dismiss();
                                    }
                                });

                                if (!node.isLeaf()) {
                                    //Update and toggle the node.
                                    onToggle(!node.isExpand(), holder);

//                    if (!node.isExpand())
//                        adapter.collapseBrotherNode(node);
                                }
                                return false;
                            }

                            @Override
                            public void onToggle(boolean isExpand, RecyclerView.ViewHolder holder) {
                                DirectoryNodeBinder.ViewHolder dirViewHolder = (DirectoryNodeBinder.ViewHolder) holder;
                                final ImageView ivArrow = dirViewHolder.getIvArrow();
                                int rotateDegree = isExpand ? 90 : -90;
                                ivArrow.animate().rotationBy(rotateDegree)
                                        .start();

                            }
                        });
                        rv.setAdapter(adapter);
                        dialogAddPost.dismiss();
                    }
                });


            }
        });
    }


    private void initView() {
        rv = (RecyclerView) findViewById(R.id.rv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.id_action_close_all:
                adapter.collapseAll();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
